async function getUserAsync(url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}
const data = getUserAsync('../jsonfiles/perYearMatches.json');
data.then(res => Highcharts.chart('container1', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'All match played in ipl season wise'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      //colors: pieColors,
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b><br>{point.y} ',
        distance: -50,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 4
        }
      }
    }
  },
  series: [
    {
      name: 'Total Match Played',
      data: Object.entries(res)
    }
  ]
}));

var pieColors = (function() {
  var colors = [],
    base = Highcharts.getOptions().colors[0],
    i;
  for (i = 0; i < 10; i += 1) {
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color
    colors.push(
      Highcharts.Color(base)
        .brighten((i - 3) / 7)
        .get()
    );
  }
  return colors;
})();

const extraRuns=getUserAsync('../jsonfiles/totalExtraRuns.json');
extraRuns.then(res=> Highcharts.chart('container3', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Total Extra Runs 2016'
  },

  xAxis: {
    title: {
      text: 'Teams'
    },
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Total Extra Runs'
    }
  },
  
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span> <b>{point.y:}</b> <br/>'
  },

  series: [
    {
      name: "Team",
      colorByPoint: true,
      data: Object.entries(res)
    }
  ],
  
}));

const bowlersEconomy = getUserAsync('../jsonfiles/getTop10EconomyBowler.json');
bowlersEconomy.then(res=>Highcharts.chart('container4', {

  title: {
      text: 'Top 10 Economy Bowler of 2015'
  },

  yAxis: {
      title: {
          text: 'Economy'
      }      
  },
  xAxis :{
    title:{
      text:'Bowler'
    },
    categories:Object.keys(res)
  },
  
  legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
  },
  series: [{
      name: 'Economy',
      data: Object.values(res)
  }],

  responsive: {
      rules: [{
          condition: {
              maxWidth: 500
          },
          chartOptions: {
              legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
              }
          }
      }]
  }

})
)

getUserAsync('../jsonfiles/matchesWonPerTeamPerYear.json').then(res=>{
 
  Highcharts.chart('container2', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Team won year wise matches'
    },
    xAxis: {
      categories: Object.keys(res)
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Total Match Played'
      }
    },
    tooltip: {
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
      shared: true
    },
    plotOptions: {
      column: {
        stacking: 'percent'
      }
    },
    series: [{
      // name: Object.keys(res),
      data:Object.values(res)
    }]
  })
});
