const { Pool } = require('pg');
const fs = require('fs');
const pool = new Pool({
    user:'developer',
    password:'123',
    database:'postgres',
    port:5432,
    host:'localhost'
});

matchPlayedPerYear=()=>{
   return pool.query("SELECT SEASON,COUNT(SEASON) AS Match_played FROM MATCH GROUP BY SEASON ORDER BY SEASON")
   
}

// calculate extra runs for every team in 2016
extraRunsPerTeam=(year)=>{
   return pool.query(`SELECT bowling_team as Team,SUM(extra_runs) as Extra_Run FROM deliveries WHERE match_id in( SELECT id FROM match WHERE season = ${year}) GROUP BY bowling_team `)
}

matchWonPerTeamPerSeason=()=>{
   return pool.query("select winner, count(winner), season from match group by season ,winner order by season")
}
matchWinnerSeasonWiseFormatting=(winnerTeams)=>{
    // console.log(winnerTeams);
    return winnerTeams.reduce((seasonWiseWinners,matchWinner)=>{
        if(!seasonWiseWinners[matchWinner.season]){
            seasonWiseWinners[matchWinner.season]={};
        }
        seasonWiseWinners[matchWinner.season][matchWinner.winner]=parseInt(matchWinner.count);
        return seasonWiseWinners;
    },{});
}
// calculate extra runs of 2015 per team
getTop10EconomyBowler=(year)=>{
  return pool.query(`SELECT bowler, CAST(SUM(total_runs)*6.000/COUNT(case when ball<=6 THEN 1 ELSE 0 END) as decimal(10,3)) as economy from deliveries where match_id in(SELECT id FROM match WHERE season=${year}) GROUP BY bowler HAVING COUNT(ball)>=29 ORDER BY economy LIMIT 10`)
    
}

formatingData=(result,key,values)=>{
    return result.reduce((objectData,curElem)=>{
     objectData[curElem[key]]=parseFloat(curElem[values])
    return objectData
      },
  {})
 
}
// creating json files
function convertToJson(file,filePath) {
    fs.writeFile(`${filePath}`, JSON.stringify(file, null, 4), (err) => {
      if (err) {
          console.log(err);
          return;
      }
      console.log(`${filePath} has been created`);
  });
  }

module.exports={
    matchPlayedPerYear,
    extraRunsPerTeam,
    getTop10EconomyBowler,
    matchWonPerTeamPerSeason,
    matchWinnerSeasonWiseFormatting,
    formatingData,
    convertToJson
}