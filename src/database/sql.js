const { Pool } = require('pg');
const fs = require("fs");

const pool = new Pool({
    user:'developer',
    password:'123',
    database:'postgres',
    port:5432,
    host:'localhost'
});
dropTable=()=>{
  return pool.query("DROP TABLE IF EXISTS deliveries,match");
}


function creatTableForDeliveries(){
    // pool.connect();
  return pool.query("CREATE TABLE deliveries(match_id INT,inning INT,batting_team VARCHAR,bowling_team VARCHAR,over INT,ball INT,batsman VARCHAR,non_striker VARCHAR,bowler VARCHAR,is_super_over INT,wide_runs INT,bye_runs INT,legbye_runs INT,noball_runs INT,penalty_runs INT,batsman_runs INT,extra_runs INT,total_runs INT,player_dismissed VARCHAR,dismissal_kind VARCHAR,fielder VARCHAR)");
}
 function insertDataIntoDeliveriesTable(){
    return pool.query("\COPY deliveries  from '/home/poonia/Desktop/ipl/src/data/deliveries.csv' with delimiter ',' csv header");
 }
function createTableForMatches(){
    // pool.connect();
   return pool.query("CREATE TABLE match(id INT,season INT,city VARCHAR,date DATE,team1 VARCHAR,team2 VARCHAR,toss_winner VARCHAR,toss_decision VARCHAR,result VARCHAR,dl_applied INT,winner VARCHAR,win_by_runs INT,win_by_wickets INT,player_of_match VARCHAR,venue VARCHAR,umpire1 VARCHAR,umpire2 VARCHAR,umpire3 VARCHAR)")
}
function insertDataIntoMatchTable(){
    pool.query("\COPY match from '/home/poonia/Desktop/ipl/src/data/matches.csv' with delimiter ',' csv header");
}
 function insertDeliveries(){
     dropTable()
     .then (()=>creatTableForDeliveries())
    //  console.log('delivery table created');
     .then(()=> insertDataIntoDeliveriesTable())
    //  console.log('delivery data inserted');
     .then(()=> createTableForMatches())
    //  console.log('match table is created');
     .then (()=>insertDataIntoMatchTable()).catch(err=>console.log(err));
     console.log('matches and deliveries data inserted and created');
 }
 insertDeliveries();