let queries = require('./query');

matchPlayedPerYear=()=>{
    let key='season'
    let values='match_played'
    let fileName="../output/matchplayedperyear.json";
    queries.matchPlayedPerYear().then(matchPlayedPerYearResponse=>{
       return queries.formatingData(matchPlayedPerYearResponse.rows,key,values)
    }).then(formattedData=>queries.convertToJson(formattedData,fileName))
    .catch(err=>console.error(err));
}
extraRunsPerTeam=()=>{
    let key='team'
    let values='extra_run'
    let fileName="../output/extraruns.json";
    queries.extraRunsPerTeam(2016).then(extraRunsPerTeamResponse=>{
        return queries.formatingData(extraRunsPerTeamResponse.rows,key,values)
    }).then(formattedData=>queries.convertToJson(formattedData,fileName)).catch(err=>console.error(err));
}
getTop10EconomyBowler=()=>{
    let key='bowler'
    let values='economy'
    let fileName="../output/bowlereconomy.json";
    queries.getTop10EconomyBowler(2015).then(bowlersEconomyResponse=>{
        return queries.formatingData(bowlersEconomyResponse.rows,key,values)
    }).then(formattedData=>queries.convertToJson(formattedData,fileName)).catch(err=>console.error(err));
}
matchWonPerTeamPerSeason=()=>{
    let fileName="../output/matchwonseasonwise.json";
    queries.matchWonPerTeamPerSeason().then(matchWonSeasonWiseResponse=>{
       return matchWinnerSeasonWiseFormatting(matchWonSeasonWiseResponse.rows)
    }).then(formattedwinerseasonwise=>queries.convertToJson(formattedwinerseasonwise,fileName))
    .catch(err=>console.error(err));
}
async function queryRunner(){
   await matchPlayedPerYear();
    await extraRunsPerTeam();
    await getTop10EconomyBowler();
    await matchWonPerTeamPerSeason();
}
queryRunner();