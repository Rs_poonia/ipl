const fs = require('fs');
let ipl = require('./ipl.js');
let extraAssessment = require('./iplExtraQuestions.js');
let matches = JSON.parse(fs.readFileSync('../data/match.json', 'utf-8'));
let dataDelivery = JSON.parse(
  fs.readFileSync('../data/deliveries.json', 'utf-8')
);
// generate json files for problem statement 1
// console.log(ipl);
fs.writeFile("../jsonfiles/perYearMatches.json", JSON.stringify(ipl.getMatchPerSeasion(matches), null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("seasonMatches File Created");
});
// generate json file for problem statement 2

console.log(extraAssessment.mostValuablePlayerPerSeason(matches));
fs.writeFile("../jsonfiles/matchesWonPerTeamPerYear.json", JSON.stringify(ipl.matchesWonPerTeamPerYear(matches), null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("matchesWonPerTeamPerYear File Created");
});
// // getting matchDetail of year 2016 and calculating extra Runs earned by per team
let matchDeliveryFor2016 = ipl.getDeliveriesDetailByYear(matches, dataDelivery, 2016);
let totalExtraRuns=ipl.calculateExtraRuns(matchDeliveryFor2016);
fs.writeFile("../jsonfiles/totalExtraRuns.json", JSON.stringify(totalExtraRuns, null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("totalExtraRuns File Created");
});
// // getting matchDetail of year 2016 for calculate top economies bowler of seasion
let deliveriesDetailForEconomy = ipl.getDeliveriesDetailByYear(matches,dataDelivery,2015);
fs.writeFile("../jsonfiles/getTop10EconomyBowler.json", JSON.stringify(ipl.calculateEconomyForTop10Bowlers(deliveriesDetailForEconomy), null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("getTop10EconomyBowler File Created");
});

// for calculating most valuable player per season
let mostValuablePlayerPerSeason=extraAssessment.mostValuablePlayerPerSeason(matches);

fs.writeFile("../jsonfiles/mostValuablePlayerPerSeason.json", JSON.stringify(mostValuablePlayerPerSeason, null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("mostValuablePlayerPerSeason File Created");
});

fs.writeFile("../jsonfiles/getWonWithToss.json", JSON.stringify(extraAssessment.getWonWithToss(matches), null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("getWonWithToss File Created");
});

fs.writeFile("../jsonfiles/bestEconomyInSuperOver.json", JSON.stringify(extraAssessment.bestEconomyInSuperOver(dataDelivery), null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("bestEconomyInSuperOver File Created");
});

fs.writeFile("../jsonfiles/getHighestDismissedPlayer.json", JSON.stringify(extraAssessment.getHighestDismissedPlayer(dataDelivery), null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("getHighestDismissedPlayer File Created");
});
let playerName = 'V Kohli';
let strikeRateOfPlayer = extraAssessment.getAllSeason(matches,dataDelivery,playerName);

// console.log(strikeRateOfPlayer);
fs.writeFile("../jsonfiles/getRunRateOfVirat-Kohali.json", JSON.stringify(strikeRateOfPlayer, null, 4), (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("getRunRateOfVirat-Kohali File Created");
});
