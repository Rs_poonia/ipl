// this function we are writing to get Number of match played per session in ipl

   function getMatchPerSeasion(dataMatch) {
    let seasionMatch = dataMatch.reduce(function(existTeam, currIndexTeam) {
      existTeam[currIndexTeam['season']] === undefined
        ? (existTeam[currIndexTeam['season']] = 1)
        : existTeam[currIndexTeam['season']]++;
      return existTeam;
    }, {});
    return seasionMatch;
  };

// this function we are writing for getting all calculation that match won by team per session

function matchesWonPerTeamPerYear(matches) {
  let matchesWonPerTeamPerYear = matches.reduce((matchesWonByTeam, match) => {
    if (!matchesWonByTeam[match['season']]) {
      matchesWonByTeam[match['season']] = {};
    }
    if (match['winner'] !== '') {
      if (!matchesWonByTeam[match['season']][match['winner']])
        matchesWonByTeam[match['season']][match['winner']] = 1;
      else matchesWonByTeam[match['season']][match['winner']]++;
    }
    return matchesWonByTeam;
  }, {});
  return matchesWonPerTeamPerYear;
}

   function getDeliveriesDetailByYear(dataMatch, dataDelivery, matchYear) {
    let matches = dataMatch.reduce((matchDeliveryOfGivenYear, iterateDataMatchForMatchDetails) => {
      if (parseInt(iterateDataMatchForMatchDetails['season']) === matchYear) {
        matchDeliveryOfGivenYear.push(iterateDataMatchForMatchDetails.id);
      }
      return matchDeliveryOfGivenYear;
    }, []);
    let matchDelivery = dataDelivery.filter(element => {
      return (
        parseInt(element.match_id) >= parseInt(matches[0]) &&
        parseInt(element.match_id) <= parseInt(matches[matches.length - 1])
      );
    });
    return matchDelivery;
  };
   function calculateExtraRuns(matchDelivery) {
    let team = matchDelivery.reduce(
      (totalExtraRunsByTeam, currIndexExtraRuns) => {
        if (
          totalExtraRunsByTeam[currIndexExtraRuns.bowling_team] === undefined
        ) {
          totalExtraRunsByTeam[currIndexExtraRuns.bowling_team] = parseInt(
            currIndexExtraRuns.extra_runs
          );
        } else {
          totalExtraRunsByTeam[currIndexExtraRuns.bowling_team] += parseInt(
            currIndexExtraRuns.extra_runs
          );
        }
        return totalExtraRunsByTeam;
      },
      {}
    );
    return team;
};

   function calculateEconomyForTop10Bowlers(deliveriesDetailForEconomy) {
    let Bowlers = deliveriesDetailForEconomy.reduce((storeEconomy, iterateIndexForCalculateEconomy) => {
        if (storeEconomy[iterateIndexForCalculateEconomy.bowler] === undefined) {
          storeEconomy[iterateIndexForCalculateEconomy.bowler] = {};
          storeEconomy[iterateIndexForCalculateEconomy.bowler]['runs'] =
            parseInt(iterateIndexForCalculateEconomy.wide_runs) +
            parseInt(iterateIndexForCalculateEconomy.noball_runs) +
            parseInt(iterateIndexForCalculateEconomy.batsman_runs);
          storeEconomy[iterateIndexForCalculateEconomy.bowler]['balls'] = 1;
          storeEconomy[iterateIndexForCalculateEconomy.bowler]['economy'] =
            (storeEconomy[iterateIndexForCalculateEconomy.bowler]['runs'] /
              storeEconomy[iterateIndexForCalculateEconomy.bowler]['balls']) *
            6;
        }
        storeEconomy[iterateIndexForCalculateEconomy.bowler]['runs'] +=
          parseInt(iterateIndexForCalculateEconomy.wide_runs) +
          parseInt(iterateIndexForCalculateEconomy.noball_runs) +
          parseInt(iterateIndexForCalculateEconomy.batsman_runs);
        storeEconomy[iterateIndexForCalculateEconomy.bowler]['balls']++;
        storeEconomy[iterateIndexForCalculateEconomy.bowler]['economy'] =
          (storeEconomy[iterateIndexForCalculateEconomy.bowler]['runs'] /
            storeEconomy[iterateIndexForCalculateEconomy.bowler]['balls']) *
          6;
        return storeEconomy;
      },
      {}
    );
    // now we get bowlers economy
    let sortedEconomyOfBowlers = Object.entries(Bowlers)
      .sort(
        (currentIndex, nextIndex) =>
          currentIndex[1].economy - nextIndex[1].economy
      )
      .filter(ball => ball[1].balls > 29)
      .slice(0, 10)
      .reduce((convertedObjectOfBowlers, top10BowlersIterateFromArray) => {
        convertedObjectOfBowlers[
          top10BowlersIterateFromArray[0]
        ] = parseFloat(top10BowlersIterateFromArray[1].economy.toFixed(2));
        return convertedObjectOfBowlers;
      }, {});
    return sortedEconomyOfBowlers;
  };

module.exports={
    getMatchPerSeasion,
    matchesWonPerTeamPerYear,
    getDeliveriesDetailByYear,
    calculateExtraRuns,
    calculateEconomyForTop10Bowlers
}


