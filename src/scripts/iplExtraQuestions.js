function mostValuablePlayerPerSeason(matches) {
  let valuablePlayer = matches.reduce((valuablePlayer, match) => {
    if (valuablePlayer[match.season] === undefined) {
      valuablePlayer[match.season] = {};
      valuablePlayer[match.season][match.player_of_match] = 1;
    } else {
      if (
        valuablePlayer[match['season']][match['player_of_match']] === undefined
      ) {
        valuablePlayer[match['season']][match['player_of_match']] = 1;
      } else {
        valuablePlayer[match['season']][match['player_of_match']]++;
      }
    }

    return valuablePlayer;
  }, {})

  let mostValuablePlayerPerSeason = Object.entries(valuablePlayer).reduce((result, entry) => {
    result[entry[0]] = Object.entries(entry[1]).sort((a,b) => b[1] - a[1])[0];
    return result;
  }, {})

  return mostValuablePlayerPerSeason;
}
// for calculating won toss with match
function getWonWithToss(matches) {
  let result = matches.reduce((wonWithToss, matchesIndex) => {
    if (
      matchesIndex.winner === matchesIndex.toss_winner &&
      wonWithToss[matchesIndex.winner] === undefined
    ) {
      wonWithToss[matchesIndex.winner] = 1;
    } else if (matchesIndex.winner === matchesIndex.toss_winner) {
      wonWithToss[matchesIndex.winner]++;
    }
    return wonWithToss;
  }, {});
  return result;
}
// find the seasons
function getAllSeason(matches, dataDelivery, playerName) {
  let seasonArray = matches
    .reduce((season, matchesIndex) => {
      if (!season.includes(matchesIndex.season)) {
        season.push(matchesIndex.season);
      }
      return season;
    }, [])
    .sort();
  let result = seasonArray.map(year => {
    return getStrikeRateBySeason(matches, dataDelivery, year, playerName);
  });
  return result;
}
// find the matches id per
function getStrikeRateBySeason(matches, dataDelivery, year, playerName) {
  let matchesId = matches.reduce((seasonWiseId, index) => {
    if (index.season == year) {
      seasonWiseId.push(index.id);
    }
    return seasonWiseId;
  }, []);
  // return matchesId;
  let strikeRate = dataDelivery.filter(
    delivery =>
      matchesId.includes(delivery.match_id) && delivery.batsman === playerName
  );
  let result = strikeRate.reduce((calculateStrikeRate, currentBall) => {
    if (calculateStrikeRate[year] === undefined) {
      calculateStrikeRate[year] = {};
      calculateStrikeRate[year]['batsman_runs'] = parseInt(
        currentBall.batsman_runs
      );
      calculateStrikeRate[year]['balls'] = 1;
      calculateStrikeRate[year]['strike-rate'] =
        (parseInt(calculateStrikeRate[year]['batsman_runs']) /
          parseInt(calculateStrikeRate[year]['balls'])) *
        100;
    } else {
      calculateStrikeRate[year]['batsman_runs'] += parseInt(
        currentBall.batsman_runs
      );
      calculateStrikeRate[year]['balls'] += 1;
      calculateStrikeRate[year]['strike-rate'] =
        (parseInt(calculateStrikeRate[year]['batsman_runs']) /
          parseInt(calculateStrikeRate[year]['balls'])) *
        100;
    }

    return calculateStrikeRate;
  }, {});
  const res = {};
  Object.entries(result).map(acc => {
    res[acc[0]] = acc[1]['strike-rate'].toFixed(2);
    return acc;
  });
  return res;
}
function getHighestDismissedPlayer(dataDelivery) {
  let dismissedPlayerList = dataDelivery
    .filter(dismissed => dismissed.player_dismissed != '')
    .reduce((dismiss, index) => {
      if (dismiss[index['player_dismissed']] === undefined) {
        dismiss[index['player_dismissed']] = 1;
      } else {
        dismiss[index['player_dismissed']]++;
      }
      return dismiss;
    }, {});
  return Object.entries(dismissedPlayerList)
    .sort((currIndex, nextIndex) => nextIndex[1] - currIndex[1])
    .slice(0, 1)
    .reduce((mostDismissed, dismissedPlayerIndex) => {
      mostDismissed[dismissedPlayerIndex[0]] = dismissedPlayerIndex[1];
      return mostDismissed;
    }, {});
}
function bestEconomyInSuperOver(dataDelivery) {
  let superOver = dataDelivery
    .filter(over => over.is_super_over != '')
    .reduce((superOverBowler, indexOfSuperOver) => {
      if (superOverBowler[indexOfSuperOver.bowler] === undefined) {
        superOverBowler[indexOfSuperOver.bowler] = {};
        superOverBowler[indexOfSuperOver.bowler]['runs'] =
          parseInt(indexOfSuperOver.wide_runs) +
          parseInt(indexOfSuperOver.noball_runs) +
          parseInt(indexOfSuperOver.batsman_runs);
        superOverBowler[indexOfSuperOver.bowler]['balls'] = 1;
        superOverBowler[indexOfSuperOver.bowler]['economy'] =
          (superOverBowler[indexOfSuperOver.bowler]['runs'] /
            superOverBowler[indexOfSuperOver.bowler]['balls']) *
          6;
      } else {
        superOverBowler[indexOfSuperOver.bowler]['runs'] +=
          parseInt(indexOfSuperOver.wide_runs) +
          parseInt(indexOfSuperOver.noball_runs) +
          parseInt(indexOfSuperOver.batsman_runs);
        superOverBowler[indexOfSuperOver.bowler]['balls']++;
        superOverBowler[indexOfSuperOver.bowler]['economy'] =
          (superOverBowler[indexOfSuperOver.bowler]['runs'] /
            superOverBowler[indexOfSuperOver.bowler]['balls']) *
          6;
      }
      return superOverBowler;
    }, {});
  let bestSuperOverPerformer = Object.entries(superOver)
    .sort(
      (currentIndex, nextIndex) =>
        currentIndex[1].economy - nextIndex[1].economy
    )
    // .filter(ball => ball[1].balls > 29)
    // .slice(0, 1)
    .reduce((convertedObjectOfBowlers, topSuperOverBowler) => {
      convertedObjectOfBowlers[
        topSuperOverBowler[0]
      ] = topSuperOverBowler[1].economy.toFixed(2);
      return convertedObjectOfBowlers;
    }, {});
  return bestSuperOverPerformer;
}

module.exports = {
  mostValuablePlayerPerSeason,
  getWonWithToss,
  getAllSeason,
  getStrikeRateBySeason,
  getHighestDismissedPlayer,
  bestEconomyInSuperOver
};
